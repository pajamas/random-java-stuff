public class Fraction
        extends Number
        implements Comparable<Fraction> {
    
    public int numerator;
    public int denominator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        reduce();
    }
    public Fraction(int value) {
        this(value, 1);
    }
    public Fraction() {
        this(0,1);
    }

    public void reduce() {
        int min = Math.min(numerator, denominator);
        if (numerator % min == 0 && denominator % min == 0) {
            numerator /= min;
            denominator /= min;
        }
        for (int i = (int) Math.sqrt(min); i > 1;) {
            if (numerator % i == 0 && denominator % i == 0) {
                numerator /= i;
                denominator /= i;
            } else {
                i--;
            }
        }
    }

    public void add(Fraction num) {
        if (denominator == num.denominator) {
            numerator += num.numerator;
        } else {
            numerator = num.denominator * numerator + num.numerator * denominator;
            denominator = num.denominator * denominator;
        }
        reduce();
    }

    public void add(int num) {
        numerator += denominator*num;
        reduce();
    }

    public void subtract(Fraction num) {
        add(new Fraction(-num.numerator, num.denominator));
        reduce();
    }

    public void subtract(int num) {
        add(-num);
        reduce();
    }
    
    public void multiply(Fraction num) {
        numerator *= num.numerator;
        denominator *= num.denominator;
        reduce();
    }
    public void multiply(int num) {
        numerator *= num;
        reduce();
    }

    public void divide(Fraction num) {
        numerator *= num.denominator;
        denominator *= num.numerator;
        reduce();
    }
    public void divide(int num) {
        denominator *= num;
        reduce();
    }

    public int intValue() {
        return numerator/denominator;
    }
    public long longValue() {
        return numerator/denominator;
    }
    public double doubleValue() {
        return numerator/denominator;
    }
    public float floatValue() {
        return numerator/denominator;
    }

    public int compareTo(Fraction num) {
        return numerator*num.denominator - num.numerator*denominator;
    }

    @Override
    public String toString() {
        if (denominator == 1) return "" + numerator;
        return numerator + "/" + denominator;
    }
}
