public class Test {
    public static void main(String[] args) {
        System.out.println("Luodaan murtoluku 1/2:");
        Fraction num1 = new Fraction(1,2);
        System.out.println(num1);
        System.out.println("Kerrotaan kolmella:");
        num1.multiply(3);
        System.out.println(num1);
        System.out.println("Kerrotaan sadalla:");
        num1.multiply(100);
        System.out.println(num1);
        System.out.println("Jaetaan 25:llä:");
        num1.divide(25);
        System.out.println(num1);
        System.out.println("Lisätään 4:");
        num1.add(4);
        System.out.println(num1);
        System.out.println("Jaetaan 7:llä:");
        num1.divide(7);
        System.out.println(num1);
        System.out.println("Miinustetaan 2:");
        num1.subtract(2);
        System.out.println(num1);
        System.out.println("Toinen murtoluku 1/22:");
        Fraction num2 = new Fraction(1,22);
        System.out.println("Lisätään alkuperäiseen lukuun:");
        num1.add(num2);
        System.out.println(num1);

    }
}
